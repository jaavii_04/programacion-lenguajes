# Lenguaje de programacion

###   Python <img src="https://seeklogo.com/images/P/python-logo-AE8E0705F6-seeklogo.com.png" alt="Python" width="90">

*While*

```python
i = 1

while i <= 10:
    print(i)
    i = i + 1
```

*For*

```python
mi_lista = [1, 2, 3, 4, 5]
for elemen in mi_lista:
    print(elemento)

```

*If*

```python
x = 10

if x > 0:
    print("x es un numero positivo")
else:
    print("x es un numero negativo")

```

### Funciones

*Funcion*

```python
def say_hello(name)
    print("Hello, " + name)

say_hello("Javier")
say_hello("Bob")
```


## Apartados

* [ GRUB Protegido ](./GRUB-Protected/)

* [ Acepta el Reto ](./AceptaElReto/)

* [ NFS ](./NFS/)

* [ Apartado de SMX de primero ](./SMX-1/)

* [ Script para buscrar IPs duplicadas ](./Utils/)

* [ Programacion de SOX (segundo) ](./SMX-SOX/)

* [ Redes de primero ](./Redes/)

* [ SAMBA ](./SAMBA/)

 
