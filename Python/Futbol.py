# Definimos una lista de preguntas y respuestas
preguntas_respuestas = [("¿Cuál es el equipo más ganador de la Champions League?", "Real Madrid"), ("¿Cuál es el equipo con más títulos de la Liga española?", "Real Madrid"), ("¿Cuál es el equipo con más títulos de la Premier League?", "Manchester United"), ("¿Cuál es el equipo más ganador de la Copa del Rey?", "Barcelona"), ("¿Cuál es el equipo más ganador de la Serie A italiana?", "Juventus") ]

# Inicializamos la puntuación del usuario en cero
puntuacion = 0

# Hacemos un bucle sobre cada pregunta y respuesta
for pregunta, respuesta_correcta in preguntas_respuestas:
    # Pedimos la respuesta del usuario
    respuesta_usuario = input(pregunta + " ")
    # Si la respuesta del usuario es correcta, aumentamos su puntuación en 1
    if respuesta_usuario.lower() == respuesta_correcta.lower():
        puntuacion += 1
    else:
        print ("La respuesta correcta es: " + respuesta_correcta )
    print ()
# Mostramos la puntuación final del usuario
print("Has respondido correctamente " + str(puntuacion) + " preguntas.")
