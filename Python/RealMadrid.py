# Lista de preguntas y respuestas
preguntas_respuestas = [ ("¿Cuántas Champions League ha ganado el Real Madrid?", "13"), ("¿Cuántos títulos de la Liga española ha ganado el Real Madrid?", "35"), ("¿Cuál es el apodo del Real Madrid?", "Los Blancos"), ("¿Cuál es el estadio del Real Madrid?", "Santiago Bernabéu"), ("¿Cuál es el número de Cristiano Ronaldo en el Real Madrid?", "7") ]

# Inicializamos la puntuación a cero
puntos = 0

# Bucle sobre cada pregunta y respuesta
for pregunta, respuesta_correcta in preguntas_respuestas:
    # Respuesta del usuario
    respuesta_usuario = input(pregunta + " ")
    # comprobacion si es correcta indiferente si esta en mayuscula o no
    if respuesta_usuario.lower() == respuesta_correcta.lower():
        puntos += 1
    else:
        print (" La respuesta correcta es " + respuesta_correcta)
# Resultado de respuestas correctas
print ("Has respondido correctamente " + str(puntos) + " pregunta.")

