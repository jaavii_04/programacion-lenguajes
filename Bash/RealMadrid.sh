# Definimos una lista de preguntas y respuestas
preguntas=("¿Cuántas_Champions_League_ha_ganado_el_Real_Madrid?" "¿Quién_es_el_máximo_goleador_de_la_historia_del_Real_Madrid?" "¿Cuál_es_el_estadio_del_Real_Madrid?" "¿Cuál_es_el_apodo_del_Real_Madrid?" "¿Quién_es_el_entrenador_actual_del_Real_Madrid?")
respuestas=("13" "Cristiano Ronaldo" "Estadio Santiago Bernabéu" "Los Blancos" "Carleto Anchelotti")
# Inicializamos la puntuación del usuario en cero
puntuacion=0
p=0
# Hacemos un bucle sobre cada pregunta y respuesta
for i in ${preguntas[@]}; do
    ia=$(echo  $i | tr "_" " ")
    # Pedimos la respuesta del usuario
    read -p "${ia} " respuesta_usuario
    # Si la respuesta del usuario es correcta, aumentamos su puntuación en 1
    if [ "$respuesta_usuario" == "${respuestas[p]}" ]; then
        echo "La respuesta es correcta"
        ((puntuacion++))
    else
        echo "No es correcto"
        echo "La respuesta correcta era "${respuestas[p]}
    fi
    ((p++))
done

# Mostramos la puntuación final del usuario
echo "Has obtenido $puntuacion puntos."
