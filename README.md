# Repositorio para programar libremente

Este repositorio vamos a utilizarlo para programar cosas que creamos de  lenguajes que creemos que son importantes 

## Lenguaje de programacion

###   Bash

* [ Carpeta de Bash  ](./Bash/)

<img src="https://lignux.com/wp-content/uploads/2018/06/bash2.png" alt="Bash" width="100"/>

###    Python

* [ Carpeta de Python ](./Python/)

<img src="https://seeklogo.com/images/P/python-logo-AE8E0705F6-seeklogo.com.png" alt="Python" width="150"/>

## AlgoDeHumor

![XKCD - Real Programmers](https://imgs.xkcd.com/comics/real_programmers.png)

## Enlaces de Interés

* [Pagina Oficial de Debian](https://www.debian.org/)
* [Pagina Oficial de Ubuntu](https://ubuntu.com/)
* [Pagina Oficial de RedHat](https://www.redhat.com/es/technologies/linux-platforms/enterprise-linux)


 
